""""""""""""""""""""""""""""""""""""""""""""""""
" common settings
""""""""""""""""""""""""""""""""""""""""""""""""
" do not keep compatible with vi
set nocp
" vim script path
if has ("win32")
    set runtimepath+=d:\dropbox\fchai\.vim,d:\dropbox\fchai\.vim\bundle\vundle
else
    set rtp+=~/.vim/bundle/vundle/
endif
" show line number
set nu
" allow backspacing over everything in insert mode
set bs=2
set ruler
set wrap
" I hate noise
set vb
" enable syntax
syntax enable
syntax on
if has("gui_running")
    colorscheme desert-gui
else
    colorscheme desert
endif
" set margin width and color
if exists('+colorcolumn')
    set cc=80
    hi ColorColumn ctermbg=lightblue guibg=lightblue
endif
" search settings
set hls
set ignorecase
" Only ignore case when we type lower case when searching
set smartcase
set incsearch
" no swap file :-D
set noswf
" set swap file dir
" set dir=/var/tmp
" Always show cursor position
set ruler
set nolinebreak
set nobackup
set mousehide
" Set to auto read when a file is changed from the outside
set autoread
" do not use mouse anywhere
set mouse=
" Sets how many lines of history VIM har to remember
set history=400
set ch=2
" only use unix file type format
set ffs=unix
set ff=unix
" paste toggle setting
set pastetoggle=<F9>
" Enable filetype plugin

" vundle!
filetype off
if has ("win32")
    call vundle#rc('d:\dropbox\fchai\.vim\bundle')
else
    call vundle#rc()
endif
Bundle 'gmarik/vundle'
Bundle 'a.vim'
Bundle 'bufexplorer.zip'
Bundle 'minibufexpl.vim'
Bundle 'The-NERD-tree'
Bundle 'taglist.vim'
Bundle 'ctrlp.vim'
Bundle 'EasyMotion'
"Bundle 'Valloric/YouCompleteMe'
Bundle 'https://github.com/Lokaltog/vim-powerline.git'
filetype plugin indent on

filetype plugin on
filetype indent on

" EasyMotion setting
let EasyMotion_keys = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'

" textile file setting
au BufRead,BufNewFile *.textile set filetype=textile

" When editing a file, always jump to the last cursor position
if has("autocmd")
  autocmd BufReadPost *
  \ if line("'\"") > 0 && line ("'\"") <= line("$") |
  \   exe "normal g'\"" |
  \ endif
endif

""""""""""""""""""""""""""""""""""""""""""""""""
" ui settings
""""""""""""""""""""""""""""""""""""""""""""""""
" use powerline!
set laststatus=2
set t_Co=256
let g:Powerline_symbols = 'compatible'
set encoding=utf8

" gVim only settings
if has("gui_running")
  " gui option = nothing, on windows, enable copy on selection by mouse
  if has ("win32")
      set go=a
  else
      set go=
        noremap <LeftRelease> "+y<LeftRelease>
  endif
  " no blink curcor, block shape curcor
  set gcr=a:blinkon0,a:block
  " auto maximum the window when vim startup
  au GUIEnter * simalt ~x
  " fonttype setting
  "set guifont=consolas:h12
  set guifont=lucida_console:h12
  " set cursorline
endif

""""""""""""""""""""""""""""""""""""""""""""""""
" key bindings settings
""""""""""""""""""""""""""""""""""""""""""""""""
" windows moving shutcarts
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

""""""""""""""""""""""""""""""""""""""""""""""""
" leader and leader bindings
""""""""""""""""""""""""""""""""""""""""""""""""
" Set mapleader
let mapleader = ","
let g:mapleader = ","
let g:EasyMotion_leader_key = '<space>'

" Fast remove highlight search
nmap <silent> <leader><cr> :noh<cr>

""""""""""""""""""""""""""""""""""""""""""""""""
" meta key bindings
""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""
" Indenting
""""""""""""""""""""""""""""""""""""""""""""""""
set sw=4
set ts=8
set expandtab
set autoindent

" indent line, show tab, etc.
"set list lcs=tab:\|\ 
set list lcs=tab:\>\ 
let g:indentLine_indentLevel = 20

""""""""""""""""""""""""""""""""""""""""""""""""
" special plugins' settings
""""""""""""""""""""""""""""""""""""""""""""""""

" NERDTree setting
let g:NERDTreeDirArrows=0

" ctags settings
let Tlist_Show_One_File=1
let Tlist_Exit_OnlyWindow=1
let Tlist_Use_Right_Window=1
if has ("win32")
    let Tlist_Ctags_Cmd='ctags-exuberant'
else
    let Tlist_Ctags_Cmd='ctags'
endif
if &diff
    let Tlist_Auto_Open=0 "don't auto pen when compare two files
else
    let Tlist_Auto_Open=1 "auto pen Tlist when open a file
endif
autocmd BufWritePost * :TlistUpdate

" gnu global settings
let GtagsCscope_Auto_Load = 1
let GtagsCscope_Auto_Map = 1
let GtagsCscope_Quiet = 1
set cscopetag

" minibufexplorer settings
nmap <silent> <C-n> :bn<cr>
nmap <silent> <C-p> :bp<cr>
" hide MBE when diff
let miniBufExplorerHideWhenDiff = 1

""""""""""""""""""""""""""""""""""""""""""""""""
" small code snippets
""""""""""""""""""""""""""""""""""""""""""""""""

" highlights the background in a subtle red for text that goes over the 80
" column limit
"highlight overLength ctermbg=red ctermfg=white guibg=#592929
"match overLength /\%81v.\+/
"match OverLength /\%>80v.\+/
match ErrorMsg '\%>80v.\+'

" toggle cuc, for alignment check.
map <leader>cc :call HilColumn()<CR>
let s:cucToggle = 0
function! HilColumn()
    if s:cucToggle == 0
        set cuc
        let s:cucToggle = 1
    else
        set nocuc
        let s:cucToggle = 0
    endif
"   let col = virtcol(".")
"   let cl  = split(&cc, ',')
"   if count(cl, string(col)) <= 0
"       execute "set cc+=".col
"   else
"       execute "set cc-=".col
"   endif
endfunction

