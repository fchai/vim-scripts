" Vim color file
" Maintainer:   Hans Fugal <hans@fugal.net>
" Last Change:  $Date: 2004/06/13 19:30:30 $
" Last Change:  $Date: 2004/06/13 19:30:30 $
" URL:      http://hans.fugal.net/vim/colors/desert.vim
" Version:  $Id: desert.vim,v 1.1 2004/06/13 19:30:30 vimboss Exp $

" cool help screens
" :he group-name
" :he highlight-groups
" :he cterm-colors

" cterm NR -> gui hex
" 0       Black         #000000
" 1       DarkRed       #bf0000
" 2       DarkGreen     #00bf00
" 3       DarkYellow    #bfbf00
" 4       DarkBlue      #0000bf
" 5       DarkMagenta   #bf00bf
" 6       DarkCyan      #00bfbf
" 7       Gray          #bfbfbf

set background=dark

let g:colors_name="desert-gui"

hi Normal   guifg=#bfbfbf guibg=Black

" highlight groups
"hi CursorIM
"hi Directory
"hi DiffAdd
"hi DiffChange
"hi DiffDelete
"hi DiffText
"hi ErrorMsg
"hi VertSplit   guibg=#bfbfbf guifg=Black gui=none
"hi VertSplit   gui=reverse
"hi Folded      guibg=Black guifg=#403639
hi Folded       gui=bold guifg=Black
hi FoldColumn   gui=bold guibg=Black guifg=#403639
hi LineNr       ctermfg=3 guifg=DarkYellow
hi ModeMsg      cterm=NONE ctermfg=brown guifg=DarkYellow gui=none
hi MoreMsg      guifg=#00bf00 gui=none ctermfg=darkgreen
hi NonText      guifg=#0000bf
hi Question     cterm=bold ctermfg=2 gui=bold guifg=#00bf00
hi Search       guifg=#bfbfbf guibg=#0000bf
hi StatusLine   gui=bold,reverse
hi StatusLineNC gui=reverse
hi Title        guifg=#bf00bf gui=none
hi Visual       gui=reverse
"hi VisualNOS
hi WarningMsg   ctermfg=1 guifg=#bf0000
"hi WildMenu
"hi Menu
"hi Scrollbar
"hi Tooltip

" syntax highlighting groups
hi Comment      guifg=#00bfbf
hi Constant     guifg=#bfbf00
hi Identifier   guifg=#00bfbf
hi Statement    guifg=#bfbf00 gui=none
hi PreProc      guifg=#bf00bf
hi Type         guifg=#00bf00 gui=none
hi Special      guifg=#bf00bf
"hi Underlined
hi Ignore       guifg=Black
"hi Error
hi Todo         guifg=Black guibg=#bfbf00

" color terminal definitions
hi SpecialKey   ctermfg=darkgreen guifg=DarkGreen
hi Directory    guifg=#00bfbf
hi ErrorMsg     gui=bold guifg=#bfbfbf guibg=#bf0000
hi IncSearch    gui=bold guifg=#00bf00 guibg=#bfbf00

"vim: sw=4
